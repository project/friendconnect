<?php

define('FRIENDCONNECT_OSAPI_SVN', 'http://opensocial-php-client.googlecode.com/svn/trunk/');
//define('FRIENDCONNECT_OSAPI_SVN', 'http://opensocial-php-client.googlecode.com/svn/trunk/src/osapi/');

/**
 * Implements hook_drush_command().
 */
function friendconnect_drush_command() {
  $items['friendconnect-dl'] = array(
    'description' => 'Download the OpenSocial PHP Library.',
    'options' => array(
      '--path' => dt('Optional. A path to the download folder. If omitted Drush will use the default location (sites/all/libraries/osapi).'),
    ),
  );
  return $items;
}

/**
 * Download the OpenSocial PHP Library from SVN.
 */
function drush_friendconnect_dl() {
  $path = drush_get_option('path', drush_get_context('DRUSH_DRUPAL_ROOT') . '/sites/all/libraries/osapi');
  if (drush_shell_exec('svn checkout ' . FRIENDCONNECT_OSAPI_SVN . ' ' . $path)) {
    drush_log(dt('OpenSocial PHP Library has been downloaded to @path.', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to download the OpenSocial PHP Library to @path.', array('@path' => $path)), 'error');
  }
}

/**
 * Implements drush_MODULE_post_COMMAND().
 */
function drush_friendconnect_post_pm_enable() {
  $modules = func_get_args();
  if (in_array('friendconnect', $modules)) {
    drush_friendconnect_dl();
  }
}
