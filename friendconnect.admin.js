
(function ($) {

Drupal.behaviors.friendConnectFieldsetSummaries = {
  attach: function (context) {
    $('fieldset#edit-friendconnect-comments', context).setSummary(function (context) {
      if ($('#edit-friendconnect-comments-enabled').attr('checked')) {
        return Drupal.t('Enabled');
      }
      else {
        return Drupal.t('Disabled');
      }
    });
    $('fieldset#edit-friendconnect-socialbar', context).setSummary(function (context) {
      if ($('#edit-friendconnect-socialbar-enabled').attr('checked')) {
        return Drupal.t('Enabled');
      }
      else {
        return Drupal.t('Disabled');
      }
    });
    $('fieldset#edit-friendconnect-newsletter', context).setSummary(function (context) {
      if ($('#edit-friendconnect-newsletter-enabled').attr('checked')) {
        return Drupal.t('Enabled');
      }
      else {
        return Drupal.t('Disabled');
      }
    });
  }
};

Drupal.behaviors.friendConnectColor = {
  attach: function (context, settings) {
    if (!$('#gfc_color_picker', context).size()) {
      return;
    }

    var p = $('#gfc_color_picker').once('color').css('opacity', 0.25);
    var farb = $.farbtastic('#gfc_color_picker');

    $('.color')
    .each(function () { farb.linkTo(this); $(this).css('opacity', 0.75); $(this).parent().find('label').addClass('color-label'); })
    .focus(function() {
      farb.linkTo(this);
      p.css('opacity', 1);
    });
  }
};

})(jQuery);
